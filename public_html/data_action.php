<?php
/*
 * 
 */
require_once(__DIR__ . "/lib/sdr_action.php");
/**
 * 
 */
class DataAction extends SdrAction{

	/*
	 * 
	 */
	protected $res = [];

	/*
	 * 
	 */
	public function execAction(Req $Req){
		

		//----------------------------------------------------------------------------
		$method = $Req->method ?: strtoupper($_SERVER['REQUEST_METHOD']);
		if(eq($_SERVER['HTTP_DEBUG_DHC'],1)){
		  $method = $_POST["method"] ?: $method;
		}

		$tbl  = $Req->tbl;
		$pkey = $Req->pkey;
		$where = $Req->where;

		/*
		 * 
		 */
		$param = [];
		foreach($Req->getRequestPostKeys() as $k=>$v){
			if(!eq($v,'pkey')){
			  $param[$v] = sdr_var2type($Req->$v);
		    }
		}
		//----------------------------------------------------------------------------
		//d(APP_NAME);
		//d($method);
		//d($_SERVER);
		//$msg = $Req->getParam();
		$res = [];
		$res['APP_ID'] = APP_ID;
		$res['APP_NS'] = APP_NS;
		$res['method'] = $method;
		$res['param'] = $Req->getParam();
		$res['tbl'] = $tbl;
		$res['pkey'] = $pkey;
		//
		if(eq($method,"GET") && v($tbl)){
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  $res['rows'] = [$ds->lookup($pkey)];
		}elseif(eq($method,"QUERY") && v($where)){
		  $where = self::parseStr($where);
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  $res['rows'] = $ds->query($where);
		}elseif(eq($method,"POST") && v($pkey) && v($tbl)){
		  $param['inymd'] = (int)date("Ymd");
		  $param["indate"]= new DateTime(date('Y-m-d H:i:s'));
		  $param['chymd'] = (int)date("Ymd");
		  $param["chdate"]= new DateTime(date('Y-m-d H:i:s'));
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  list($res['status'], $res['rows']) = $ds->insert($pkey, $param);
		  
		}elseif(eq($method,"PUT") && v($pkey) && v($tbl)){
		  $add = ["indate"=>new DateTime(date('Y-m-d H:i:s')), 'inymd' => (int)date("Ymd")];
		  $param["chdate"]=new DateTime(date('Y-m-d H:i:s'));
		  $param["chymd"] = (int)date("Ymd");
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  list($res['status'], $res['rows']) = $ds->update($pkey, $param, $add);
			
		}elseif(eq($method,"DELETE") && v($pkey) && v($tbl)){
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  list($res['status'], $res['rows']) = $ds->delete($pkey);
		  
		}elseif(eq($method,"HIST")){
		  $pkey = sprintf('%1$s-%2$s', $param['pk_user'], time());
		  $param["indate"]=new DateTime(date('Y-m-d H:i:s'));
		  $param["inymd"] = (int)date("Ymd");
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  list($res['status'], $res['rows']) = $ds->insert($pkey, $param);
		  $res['pkey'] = $pkey;
		}
		
		
		if($res['rows']){
		  $res['rows'] = self::screening($res['rows']);
		}
		
		$this->res = $res;
	}

	/*
	 * 
	 */
	public static function main(){
		
		$Req = Req::getNew();
		$Req = Req::decParam($Req);

		$inst = new self();
		$inst->execAction($Req);
		$inst->printJson();

	}

	/*
	 * 
	 */
	private static function parseStr($where0){
		  $where = [];
		  parse_str($where0, $where);
		  
		  foreach($where as $k=>$v){
		  	$where[$k] = sdr_var2type($v);
		  }
		  return $where;
	}
	/*
	 * 
	 */
	private static function screening($rows0){
	  $rows = [];
	  foreach($rows0 as $k=>$v){
	  //	var_dump($v);
	  	if(is_array($v)){
		  	$row = [];
		  	foreach($v as $k2=>$v2){
		  		if($k2 != "indate" && $k2 != "chdate" && $k2 != "inymd" && $k2 != "chymd"){
		  			$row[$k2] = $v2;
		  		}
		  	}
		  	$rows[] = $row;
  	    }

	  }	
	  return $rows;
	}
} // EOC
// EOP