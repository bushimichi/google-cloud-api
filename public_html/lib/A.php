<?
/** ArrayObjectを拡張したデータ保持用クラス
 *  配列とハッシュを組み合わせたような制御ができる
 *
 *
 **/
class A extends ArrayObject{

 private $escape = false;
 private $param = array();

/**
 *
 *
 *
 **/
  public function __construct($input=array(), $flags=null){ if(empty($input)){ $input = array();}

    $flags || $flags = ArrayObject::ARRAY_AS_PROPS; // | ArrayObject::STD_PROP_LIST;
    parent::__construct($input, $flags, 'AIterator');
    
  }

/**
 *
 *
 *
 **/
 public function getParam(){
   return $this->param;
 }

/**
 *
 *
 *
 **/
 public function setParam($param){
   $this->param = $param;
   return $this;
 }

/**
 *
 *
 *
 **/
 public function del($k){
   unset($this->param[$k]);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function set($k, $v){
   $this->param[$k] = $v;
   return $this;
 }

/**
 *
 *
 *
 **/
 public function get($k){
  return isset($this->param[$k]) ? $this->param[$k] : (isset($this[$k]) ? $this[$k] : null);
 }

/**
 *
 *
 *
 **/
 public function __wakeup(){
   $this->__construct((array)$this);
 }

/**
 *
 *
 *
 **/
 public function __reset(){
   $this->__construct(array_values((array)$this));
 }

/**
 *
 *
 *
 **/
 public function dump(){
   return var_export($this, true);
 }

/**
 *
 *
 *
 **/
 public function add($obj){ if($obj === null){ return $this;}
   foreach($obj as $k=>$v){
     $this[] = $v;
   }
   return $this;
 }

/**
 *
 *
 *
 **/
 public function merge($obj){ if($obj === null){ return $this;}
   foreach($obj as $k=>$v){
     $this[$k] = $v;
   }
   return $this;
 }

/**
 *
 *
 *
 **/
 public function mapping($obj, $key){
 
   foreach($this as $k=>$v){
     if(isA($v) && isset($obj[$v->$key])){
       $v->merge($obj[$v->$key]);
     }
   }
   return $this;
 }

/**
 *
 *
 *
 **/
 public function each($func){
   if(!is_callable($func)){
     return $this;
   }

   foreach($this as &$v){
     $func($v);
   }
   return $this;
 }

/**
 *
 *
 *
 **/
 public function toArray(){
  return (array)$this;
 }

/**
 *
 *
 *
 **/
 public function values($key=null){
   if($key){
     $tmp = new A();
     foreach($this as $k=>$v){
       $tmp[] = $v->$key;
     }
     return $tmp;
   }else{
     return new A( array_values((array)$this) );
   }
 }

/**
 *
 *
 *
 **/
 public function keys(){
   return new A( array_keys((array)$this) );
 }

/**
 *
 *
 *
 **/
 public function group($key){
   $grp = new A();
   foreach($this as $k=>$v){
     $grp[$v->$key] = isset($grp[$v->$key]) ? $grp[$v->$key] : new A();
     $grp[$v->$key][] = $v;
   }
//   $this->__construct($grp);
   return $grp;
 }

/**
 *
 *
 *
 **/
 public function convert($cols, $emp=true){
   $tmp = new A();
   $ary = new A();
   $i = 1;

   foreach($this as $k=>$v){
     if($i % $cols === 0){
       $ary[$k] = $v;
       $tmp[] = $ary;
       $ary = new A();
     }else{
       $ary[$k] = $v;
     }
     $i++;
   }

   if($emp && v($ary)){
	   $d = $cols - $ary->count();
	   if($d > 0){
	     for($j=0;$j<$d;$j++){
	       $ary[] = a();
	     }
	   }
     $tmp[] = $ary;
   }elseif(v($ary) && $ary->count() > 0){
     $tmp[] = $ary;
   }

   $tmp->param = $this->param;
   
   return $tmp;
 }

/**
 *
 *
 *
 **/
 public function first(){
   return reset($this); //[0];
 }

/**
 *
 *
 *
 **/
 public function unshift($obj){ if($obj === null){ return $this;}
    $temp = (array)$this;
   array_unshift($temp, $obj);
   $this->__construct($temp);   
   return $this;
 }

/**
 *
 *
 *
 **/
 public function shift(&$ret=null){
    $temp = (array)$this;
    $ret = array_shift($temp);
    $this->__construct($temp);
    return $this;
 }

/**
 *
 *
 *
 **/
 public function join($v=''){
   return join((array)$this, $v);
 }

/**
 *
 *
 *
 **/
 public function kjoin($key, $sep=''){
   $temp = array();
   foreach($this as $k=>$v){
     $temp[] = $v->$key;
   }
   return join($temp, $sep);
 }

/**
 *
 *
 *
 **/
 public function last(){
   return end($this);
 }

/**
 *
 *
 *
 **/
 public function count(){
   return count((array)$this);
 }

/**
 *
 *
 *
 **/
 public function put($key, $val){
   $this[$key] = $val;
   return $this;
 }

/**
 *
 *
 *
 **/
 public function shuffle(){
   $temp = (array)$this;
   shuffle($temp);
   $this->__construct($temp);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function slice($offset, $limit, $bool=false){
   //$temp = (array)$this;
   $temp = array_slice((array)$this, $offset, $limit, $bool);
   $this->__construct($temp);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function usort($sortTool, $args=array()){
   $temp = (array)$this;
   $args = (array)$args;
   array_unshift($args, $temp);
   $temp = call_user_func_array((array)$sortTool, $args);
   $this->__construct((array)$temp);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function rsort(){
   $temp = (array)$this;
   rsort($temp);
   $this->__construct($temp);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function arsort(){
   $temp = (array)$this;
   arsort($temp);
   $this->__construct($temp);
   return $this;
 }
 
/**
 *
 *
 *
 **/
 public function sort(){
   $temp = (array)$this;
   sort($temp);
   $this->__construct($temp);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function asort(){
   $temp = (array)$this;
   asort($temp);
   $this->__construct($temp);
   return $this;
 }
/**
 *
 *
 *
 **/
 public function ksort(){
   $temp = (array)$this;
   ksort($temp);
   $this->__construct($temp);
   return $this;
 }

/**
 *
 *
 *
 **/
 public function krsort(){
   $temp = (array)$this;
   krsort($temp);
   $this->__construct($temp);
   return $this;
 }


/** 入力配列から探知鵜のカラムの値を返す
 *
 *
 *
 **/
 public function column($key){
   $temp = array();
   foreach($this as $k=>$v){
     $temp[] = $v->$key;
   }
   return new A($temp);
 }

/**  含まれているかどうか
 *
 *
 *
 **/
 public function in($key){
   $temp = (array)$this;
   return in_array($key, $temp);
 }

/**
 *
 *
 *
 **/
 public function find($key, $val){
   foreach($this as $k=>$v){
     if(eq($v->$key, $val)){ return $v;}
   }
 }

/**
 *
 *
 *
 **/
 public function filter($filterTool, &$temp2=null){
   $args = count($filterTool) > 2 ? array($filterTool[0], $filterTool[1]) : $filterTool;
   $temp2 = clone $this;
   $temp = (array)$this;
   //d('filter================>','args==============>',$args, $filterTool);
   $temp = array_filter($temp, $args);
   
   $this->__construct((array)$temp);
   if(isset($filterTool[2]) && $filterTool[2] === true){ $this->__reset(); }
   return $this;
 }

/**
 *
 *
 *
 **/
 public function remove($val){
   foreach($this as $k=>$v){
     if(eq($val, $v)){ 
       unset($this[$k]);
       break;
     }
   }
   return $this;
 }

/**
 *
 *
 *
 **/
 public function sum($key=null){
   if($key){
     $sum=0;
     foreach($this as $k=>$v){
       $sum += alt($v->$key,0);
     }
     return $sum;
   }else{
     return array_sum((array)$this);
   }
 }

/**
 *
 *
 *
 **/
 public function offsetGet($key){

   if(!(is_string($key) || is_numeric($key))){
     error_log('Illegal offset type is ' . $key. ' in '.$_SERVER['REQUEST_URI']);
   }
   
   $val = (!is_null($key) && isset($this[$key])) ? parent::offsetGet($key) : null;
   
   if(is_numeric($val)){
     if(is_int($val)){
       return (int)$val;
     }elseif(is_float($val)){
       return (float)$val;
     }
   }elseif(is_string($val)){
        if($this->getEscape()){
          return html($val);
        }
   }elseif($val instanceof A){
     $val->setEscape($this->getEscape());
   }

   return $val;
 }

/** 改行を<BR>に変換してリターン
 *
 *  @param $key 
 *  @return string
 **/
 public function br($key){
   return nl2br("{$this[$key]}");
 }

/**
 *
 *
 *
 **/
 public function setEscape($flg=true){
   $this->escape = $flg;
 }

/**
 *
 *
 *
 **/
 public function getEscape(){
   return $this->escape;
 }


/**
 *
 *
 *
 **/
 public function getIterator(){
   $it = parent::getIterator();
   if(method_exists($it, 'setEscape')){
     $it->setEscape($this->getEscape());
   }
   return $it;
 }

/**
 *
 *
 *
 **/
 public function getVal($key){
   return $this->offsetGet($key);
 }

/**
 *
 *
 *
 **/
 public function __toString(){
   $str = array();
   foreach($this as $k=>$v){
     $str[] = $k.':'.$v;
   }
   return join($str, ' ');
//   return var_export($this, true);
 }

/**
 *
 *
 *
 **/
 public function ret($bool){
   return v($bool) ? $this->first() : $this;
 }

/**
 *
 *
 *
 **/
 public static function def($pkey){
   $f = false;
   if(is_numeric($pkey) || is_string($pkey)){
     $pkey = a($pkey);
     $f = true;
   }
   return array($f, $pkey);
 }

} // end_of_class

/**
 *
 *
 *
 **/
class Vars extends A{
  private $escape = true;

/**
 *
 *
 *
 **/
 public function getEscape(){
   return $this->escape;
 }
/**
 *
 *
 *
 **/
 public function setEscape($flg=true){
   $this->escape = $flg;
 }
 
}

/**
 *
 *
 *
 **/
class AIterator extends ArrayIterator{

 private $escape = false;

/**
 *
 *
 *
 **/
 public function setEscape($flg){
   $this->escape = $flg;
 }

/**
 *
 *
 *
 **/
 public function getEscape(){
   return $this->escape;
 }

/**
 *
 *
 *
 **/
 public function current(){
   $obj = parent::current();
   if($obj instanceof A){
     $obj->setEscape($this->getEscape());
   }
   return $obj;
 }
 
}

