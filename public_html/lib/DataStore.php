<?php
/*
 * 
 */
require('../vendor/autoload.php');

//# Imports the Google Cloud client library
use Google\Cloud\Datastore\DatastoreClient;

/** 
 *  Google DataStore Access Class
 *
 *
 **/
class DataStore {
/**
 * 
 */
 private $projectId = null; // application or project ID
 private $namespaceId = null; // namespace default
 private $kind = null; // kind
 
 private $datastore = null;
 
/**
 *
 *
 *
 **/
  public function __construct($projectId, $namespaceId, $kind)
  {
  	$this->kind = $kind;
  	$this->namespaceId = $namespaceId;
    $this->projectId = $projectId;

	# Instantiates a client
	$this->datastore = new DatastoreClient([
	    'projectId' => $projectId,
		'namespaceId' => $namespaceId,
	]);    
    
  }
/*
 * 
 */
 public function query($where){
//var_dump($where);
//	$this->datastore->key($pkey)
    $query = $this->datastore->query();
    $query->kind($this->kind);
    foreach($where as $k=>$v){
	  $query->filter($k,'=',$v);
	}
	
	$rows = $this->datastore->runQuery($query);
	$res = [];
	foreach ($rows as $row) {
		$key = $row->key();
	    $obj = $row->get();
	    $obj['pkey'] = $key->pathEndIdentifier();
	    $res[] = $obj;
	}	
	return $res;
 }
/*
 * 
 */
 public function lookup($pkey){
   $taskKey = $this->datastore->key($this->kind, $pkey);
   $entity = $this->datastore->lookup($taskKey);
   if(!is_null($entity)){
	   $ret = $entity->get();
	   $ret['pkey'] = $pkey;
	   return $ret;
   }
 }

/*
 *  @param $pkey string
 *  @param $entity array
 */
 public function update($pkey, array $entity, $add){
   
   $upflg = 0;
   $entity0 = $this->lookup($pkey);
   if(!is_null($entity0)){
     foreach($entity as $k=>$v){
     	if(is_null($entity0[$k]) || $entity0[$k] != $v){
     	  $entity0[$k] = $v;
     	  $upflg = 2;
 	    }
     }
     $entity = $entity0;
   }else{
   	 foreach($add as $k=>$v){
   	 	$entity[$k] = $v;
   	 }
   	 $upflg = 1;
   }
   
   $entity = self::screening($entity);
   
   $rows = [];
   if($upflg){
	   $taskKey = $this->datastore->key($this->kind, $pkey);
	   $task = $this->datastore->entity($taskKey, $entity);
	   $this->datastore->upsert($task);
	   $rows[] = ["pkey"=>$pkey];
   }
   return [$upflg, $rows];
 }
/*
 *  @param $pkey string
 *  @param $entity array
 */
 public function insert($pkey, array $entity){
   $entity = self::screening($entity);
   $entity0 = $this->lookup($pkey);
   $flg = 0;
   $rows = [];
   if(is_null($entity0)){
	   $taskKey = $this->datastore->key($this->kind, $pkey);
	   $task = $this->datastore->entity($taskKey, $entity);
	   $this->datastore->insert($task);
	   $flg = 1;
	   $rows[] = ["pkey"=>$pkey];
   }else{
   	$rows[] = ["pkey"=>$entity0['pkey']];
   }
   return [$flg, $rows];
 }
/*
 * 
 */
 public function delete($pkey)
 {
   $entity0 = $this->lookup($pkey);
   $rows = [];
   $flg = 0;
   if(!is_null($entity0)){
     $taskKey = $this->datastore->key($this->kind, $pkey);  
     $this->datastore->delete($taskKey);
     $rows[] = $entity0;
     $flg = 1;
   }
   return [$flg, $rows];
 }
 
/*
 * 
 */
 private static function screening($entity){
 	$entity1 = array();
 	if(is_null($entity)){ return $entity1; }
 	
 	foreach($entity as $k=>$v){
 		if($k != "pkey"){
 			$entity1[$k] = $v;
 		}
 	//	var_dump([$k=>gettype($v)]);
 	}
 	return $entity1;
 }
 
}

