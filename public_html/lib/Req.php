<?
/** リクエストパラメータを一括処理するクラス
 *
 * $_GET $_POST $_FILE データを内部で扱いやすいように変換する
 *
 *  
 *  @author Intelligence Technology, Inc
 *  @version 1.0
 *
 */
class Req {

  const SDR_REQ_FILES_KEY = '__sdr_req_files_key';

/**
 *
 *
 *
 **/
  private static $instance = null;

  private $jsonFlg = false;

 /** スタティックにインスタンスを生成
  *
  *  Singleton Model でインスタンスが1つしか生成しないようにする
  *
  **/
  public static function getNew($flg=false)
  {
    if(self::$instance === null || $flg){
      self::$instance = new Req();
    }
    
    return self::$instance;
  }


 /** Parameter Set Variable
  *
  **/
  protected $param = array();
  protected $requestGetKeys = array();
  protected $requestPostKeys  = array();

  
 /**
  *
  *  Construct
  *
  **/
  private function __construct(){
    // Request Parameter Initialize
      
    $this->__initGetParameter();
    $this->__initPostParameter();
    $this->__initFileParameter();
  }

 /**  Getパラメータの初期化
  *
  *
  *
  **/
  private function __initGetParameter(){

    foreach($_GET as $k=>$v){
      $this->param[$k] = $this->chVarType($v);
      $this->requestGetKeys[] = $k;
    }
  }

 /** Postパラメータの初期化
  *
  *
  *
  **/
  private function __initPostParameter(){

    $jsonInput= file_get_contents('php://input','r');
 //   $jsonInput = fgets($handle);
 //   d($jsonInput);
    if(v($jsonInput)){
      $json = json_decode($jsonInput, true, 512, JSON_BIGINT_AS_STRING);
 //     d($json);
      if(v($json) && (is_array($json) || is_object($json))){
	      foreach($json as $k=>$v){
	        $_POST[$k] = $v;
	      }
      }
    }

    foreach($_POST as $k=>$v){
      $this->param[$k] = $this->chVarType($v);
      $this->requestPostKeys[] = $k;
    }
    
  }

 /**
  *
  *
  *
  **
  private static function staticChVarType($v){
  
    if(is_numeric($v)){
      $v = trim($v);
      if((string)$v === (string)(int)$v){
        return intVal($v);
      }elseif((string)$v === (string)(float)$v){
        return floatVal($v);
      }
      return $v;
    }elseif(is_string($v)){
      $v = trim($v);
      //return Sdr::getJsonFlg() ? $v : strip_tags($v);
      return $v;
    }elseif(is_array($v)){
      return $v;
    }
    return null;
  }
  */
 /*
  * 
  */
  public function chVarType($v){
  	return sdr_var2type($v);
  }

 /** アップロードファイルの処理
  *
  *  画像（jpg jpeg png gif）は定型のパラメータを処理する
  *  一般ファイル（pdf,csv,zip,xls,ppt,doc）は、○○○処理する
  *
  **/
  private function __initFileParameter(){
    
    if(is_array($_FILES)){
      //$S_FILES = $_SESSION[SDR_REQ_FILES_KEY] ? $_SESSION[SDR_REQ_FILES_KEY] : array();
      
      foreach($_FILES as $k=>$v){
      
        if($this->param[$k .'_del'] === '1'){
          
          $S_FILES[$k] = null;
          unset($S_FILES[$k]);
          foreach($v as $k1=>$v1){
            $this->param[$k.'_'.$k1] = null;
            unset($this->param[$k.'_'.$k1]);
          }
          
        }else{

          if(is_uploaded_file($v["tmp_name"])){

            $pt = pathinfo($v["tmp_name"]);
            $v['basename'] = $pt['basename'];
          
            //$v['name'] = strtr($v['name'], array('|'=>'｜')); // 区切り文字エスケープ
            $v['new'] = true;
            
            list($v['isimg'], $v['ext']) = ImgParse::getMimeType($v['type']);
            
            
            if(!$v['isimg']){
              list($v['isimg'], $v['ext']) = ImgParse::getImgType($v['name']);
            }
            
            if($v['isimg']){
              $im = call_user_func('imagecreatefrom'. $v['ext'], $v['tmp_name']);
              if(v($im)){
                list($v['w'], $v['h'], $v['inttype'], $v['imgtag']) = getimagesize($v['tmp_name']);
              }else{ // 読み取れる画像でないため処理せず
              //  var_dump($v);
              //  continue;
              }
            }

            //$fp = file_get_contents($v['tmp_name']);
            $fp = move_uploaded_file($v['tmp_name'], mkPath(WWW_ROOT.'/'.SDR_TMP_DIR.'/'.$v['basename']));            
            if(v($fp)){
              //$v['string'] = join(array($v['ext'], $v['name'],$v['size'],$v['w'],$v['h'], base64_encode($fp)), '|');
//              $v['text'] = base64_encode($fp);
              $v['tmp_name'] = mkPath('/'.SDR_TMP_DIR.'/'.$v['basename']);
              foreach(array('del','name','tmp_name','w','h','inttype','imgtag','ext','isimg','basename','size','new') as $k1){
                $this->param[$k.'_'. $k1] = $v[$k1];
              }
              
              //$S_FILES[$k] =  $v ;
            }

            //move_uploaded_file($v['tmp_name'], mkPath(WWW_ROOT.'/'.SDR_TMP_DIR.'/'.$v['basename']));
            
          }else{
            // EB::Log($v, 'upload file is faild');
          }

        }
      }
      // $_SESSION[SDR_REQ_FILES_KEY] = $S_FILES ;
      
    }
  }


/** 直接タグを含むパラメータを取得する
 *
 *  @param $k   string リクエストパラメータ名
 *  @param $alw string 含んでもOKなタグ ex. "<a><b>"
 *
 */
  public function getTag($k, $alw=''){
    return strip_tags($_REQUEST[$k], $alw);
  }

 /**
  *
  *
  *
  **/
 public function getParam(){

   return $this->param;
 }

/**
 *
 *
 *
 **/
 public function getRequestGetKeys(){
   return $this->requestGetKeys;
 }
/**
 *
 *
 *
 **/
 public function getRequestPostKeys(){
   return $this->requestPostKeys;
 }

/**
 *
 *
 *
 **/
 public function setRequestPostKeys($param){
   $this->requestPostKeys = $param;
   return $this;
 }

 /**
  *
  *  @param $param array
  *
  **/
 public function setParam(array $param){
   $this->param = $param;
 }

 /**
  *
  *
  *
  **/
 public function addString($param){
   foreach($param as $k=>$v){
     if(is_string($v) || is_numeric($v)){
       $this->param[$k] = $v;
     }
   }
 }
 
 /**
  *
  *  @param $param array
  *
  **/
 public function addParam(array $param){
   foreach($param as $k=>$v){
     $this->param[$k] = $v;
   }
 }
 
 /**
  *
  *
  *
  **/
  public function __get($k){
    return isset($this->param[$k]) ? $this->param[$k] : '';
  }

 /**
  *
  *
  *
  **/
  public function __set($k, $v){
    $this->param[$k] = unhtml($v);
  }


 /**
  *
  *
  *
  **/
  public function put($k, $v){
    $this->param[$k] = $v;
  }

 /**
  *
  *
  *
  **/
  public function del($k){
    unset($this->param[$k]);
  }

 /**
  *
  *
  *
  **/
  public function is($k){
    return isset($this->param[$k]);
  }
  
/**
 *
 * HTTP Redirect
 * @param $url string RedirectURL
 * @param $flg boolean HTML Redirect or Header Redirect
 *
 **/
 public static function redirect($url, $sec=0){ if(defined('TEST_STestAction_FLG') && TEST_STestAction_FLG === true){ echo $url; return;}

    if(ini_get('session.use_trans_sid')){
	    if(!isset($_COOKIE[session_name()])){ // Cookie あり
	      $pu = parse_url($url);
	      if($pu['query']){
	        $url .= '&'.session_name().'='.session_id();
	      }else{
	        $url .= '?'.session_name().'='.session_id();
	      }
	    }
    }

    if($sec){
      echo <<<_END
        <html lang="ja">
          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta http-equiv="refresh" content="$sec;url=$url">
          </head>
          <body>自動的に切り替わらない場合は<a href="$url">こちらをクリック</a>
          </body>
        </html>
_END;
       sdr_exit();
    }else{
       header('Location: ' . $url);
       sdr_exit();
    }

  }

  
 /**
  *
  *
  *
  **/
  public function getFileParam($key){
      $S_FILES = $_SESSION[self::SDR_REQ_FILES_KEY] ? $_SESSION[self::SDR_REQ_FILES_KEY] : array();
            
      if(v($S_FILES[$key])){        
        return new A($S_FILES[$key]);
      }else{
        return array();
      }
  }
  
 /**
  *
  *
  *
  **/
  public function setFileParam($key,DicImpl $obj){
  
      if(v($obj)){
        
        if(v($obj->name) && v($obj->text) ){

          $obj = $obj->getParam();
          $this->put($key.'_del', 0);
          $this->put($key.'_name', $obj['name']);      
    
          $S_FILES = $_SESSION[self::SDR_REQ_FILES_KEY] ? $_SESSION[self::SDR_REQ_FILES_KEY] : array();
          $S_FILES[$key] = $obj;
          $_SESSION[self::SDR_REQ_FILES_KEY] = $S_FILES;
          
        }else{
          //
          if(SDR_DEBUG_FLG === true){
            echo '<h3> Not found key $Req->setFileParam '. $key .'</h3>';
            //appdump($obj);
            exit;
          }
        }
      }
  }

  
 /**
  *
  *
  *
  **/
  public function getValues($key, $d=' '){
    $pat = '/'. $key . '_([0-9]+)/';
    $csv = array();
    foreach($this->param as $k=>$v){
      if(preg_match($pat, $k, $r)){
        $csv[$r[1]] = $v;
      }
    }
    ksort($csv, SORT_NUMERIC);

    if(is_string($d)){
      return join($csv, $d);
    }elseif(is_bool($d) && $d === true){
      return new A($csv);
    }
  }

 /**
  *
  *
  *
  **/
  public function qs(){
    $args = func_get_args();
    $qs = array();
    foreach($args as $k){
      if(isset($this->param[$k]) && v($this->param[$k])){
        $qs[$k] = $this->param[$k];
      }
    }
//    d('Req.qs',$qs,$args);
    return http_build_query($qs);
//    return join($qs, '&');
  }


/**
 *
 *
 *
 **/
public static function decParam($Req){
   
   if(eq(API_VER,2) && v($Req->p)){
   //  d($Req->p);
     $p = base64dec($Req->p);
   //  d($p);
     parse_str($p, $param);
   //  d($param);
     
     $Req->del('p');
     foreach($param as $k=>$v){
       $Req->put($k, $v);
     }

     if(v($Req->postData)){
     
       $postData = base64dec($Req->postData);
       parse_str($postData, $param2);
       $Req->del('postData');
       $Req->setRequestPostKeys(array_keys($param2));
       
	     foreach($param2 as $k=>$v){
         $Req->put($k,trim($v));
	     }
     }
     
   }

   return $Req;
 }
  
}
