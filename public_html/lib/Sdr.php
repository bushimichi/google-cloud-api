<?php
/*
 * 
 */
require_once(__DIR__ . '/func.php');
require_once(__DIR__ . '/A.php');
require_once(__DIR__ . '/Req.php');
require_once(__DIR__ . '/DataStore.php');


$API_VER = isset($_SERVER['HTTP_API_VERSION']) ? $_SERVER['HTTP_API_VERSION'] : 1;
define('API_VER', $API_VER);

//----------------------------------------------------------------------------
// parse Path
if(preg_match('/([^\/]+)\/([^\/]+)\.json/', $_SERVER['REQUEST_URI'], $r)){
	define('APP_PATH', $r[1]);
	define('APP_ACT', $r[2]);
}
//
if( defined('APP_PATH')){
  require_once(__DIR__ . "/../" .APP_PATH . "/sdr.def");
}
