<?php
/*
 * 
 */
class SdrAction{
	/*
	 * 
	 */
	protected $res = [];
	
	/*
	 * 
	 */
	public function __construct(){
		
	}
	
	/*
	 * 
	 */
	public function printJson($res=null)
	{
		$res = $res ?: $this->res;
		/*
		 * 
		 */
		header('Api-Version: ' . API_VER);
		
		/*
		 * 
		 */
		if(eq($_SERVER['HTTP_DEBUG_DHC'],1)){
		   	 header("Content-Type: application/json; charset=utf-8");
		     echo json_encode($res,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);	
		}else{
		
		   if(eq(API_VER,2)){
		     $json = json_encode($res,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
		     $json = urlencode($json);
		     $json2 = base64enc($json);
		     header("Content-Type: text/plain; charset=utf-8");
		     echo $json2;
		   }else{
		   	 header("Content-Type: application/json; charset=utf-8");
		     echo json_encode($res,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		   }
		}
		
	}
	
}