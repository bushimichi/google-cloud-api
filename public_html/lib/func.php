<?
/**
 *
 *
 *
 **/
 define('SDR_FUNC_ENC', 'UTF-8');
 
/** Returns an A class of all the given paramters.
 *
 * Example:
 *
 * `a('a', 'b')`
 *
 * Would return:
 *
 * `new A( array('a', 'b')) `
 *
 *  @return A Object of given paramters
 **/
 function a(){
   return new A(func_get_args());
 }

/**
 *
 *
 *
 **/
 function aj(){
   $a = new A(func_get_args());
   return $a->join(':');
 }

/**
 *
 *
 *
 **/
 function ex($sep, $str){
   return new A(explode($sep, $str));
 }

/** Default
 *
 *  @param $a mix
 *  @param $b mix
 *  @param $f format
 *  @return mix
 **/
 function alt($a, $b, $f=null){

   if(v($a) && !is_object($a)){
     if($f){
       $ret = sprintf($f, $a);
     }else{
       $ret = $a;
     }
   }elseif(v($a) && is_object($a) && $a->__toString()){
     $ret = sprintf($f, $a->__toString());
   }else{
     $ret = $b;
   }

   if(is_numeric($ret)){
     return (float)$ret;
   }else{
     return $ret;
   }
 }

/**
 *
 *
 *
 **/
 function arg($arg){
   if(is_numeric($arg) || is_string($arg)){
     $arg = a($arg);
     $arg->set('cnt', 1);
     return $arg;
   }elseif(is_array($arg)){
     return new A($arg);
   }elseif(isA($arg)){
     return new A($arg);
   }
   return $arg;
 }

/**
 *
 *
 *
 **/
 function ret($A, $arg, $def=null){
   if(v($A)){
     if(eq($arg->get('cnt'),1)){
       return $A->first();
     }else{
       return $A;
     }
   }elseif(v($def)){
     return $def;
   }
   return $A;
 }
 

/**
 *
 *
 *
 **/
 function plus($a, $b){
   return alt($a, '', '%1$s'.$b);
 }

/**
 *
 * @see debug()
 * @param mixi
 *
 **/
 function d(){
   //if(defined('SDR_DEBUG_FLG') && SDR_DEBUG_FLG === true){
   if(true){
     $dbt =  debug_backtrace();
     $args = $dbt[0];
     $args['REQUEST_URI']     = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
     $args['HTTP_USER_AGENT'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
     $args['HOST'] = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
     $args['PLNAME'] = defined('PLNAME') ? PLNAME : null;
     $args['HOSTNAME'] = function_exists('apache_getenv') ?  apache_getenv("HOSTNAME") : 'localhost';
     error_log(print_r($args,true));
   }
 }

/**
 *
 *  @param  
 *  @return 
 **/
 function l(){
   static $log=array();
   $args = func_get_args();
   if($args){
     $dbt = debug_backtrace();
     $args = $dbt[0];
     $log[] = $args;//print_r($args, true);
   }
   return $log;
 }
 

/** Constructs associative A class from pairs of arguments.
 *
 * Example:
 *
 * `h('a','b')`
 *
 * Would return:
 *
 * `new A(array('a'=>'b')`
 *
 **/
 function h(){
   $args = func_get_args();
   $a = a();
   for($i=0; $i < floor(count($args)/2); $i++){
     $a[$args[$i*2]] = $args[$i*2+1];
     
   }
   return $a;
 }

/** htmlspecialchars alias
 *
 *
 *
 **/
 function html($str){
   if(is_numeric($str)){
     return $str;
   }

   return htmlspecialchars($str, ENT_QUOTES, SDR_FUNC_ENC);
 }

/** html_entity_decode alias
 *
 *
 *
 **/
 function unhtml($str){
   if(is_numeric($str)){
     return $str;
   }
   return html_entity_decode($str, ENT_QUOTES, SDR_FUNC_ENC);
 }

/**
 *
 * Example:
 *
 * `j('a','b','c')`
 *
 * Would return:
 *
 * `'abc'`
 *
 **/
 function j(){
   return join(func_get_args());
 }



/**
 *
 * @param mixed $v 
 * @return boolean 
 *
 **/
 function v($v, $k=null){
   if(is_bool($v)){
     return $v;
   }elseif(is_numeric($v)){ // all numeric (0 and '0') are true too
     return true;
   }elseif(is_string($v)){
     return !empty($v);
   }elseif(is_array($v)){
     if(is_string($k)){
       return isset($v[$k]) && v($v[$k]);
     }else{
//       $v = array_filter($v,"v");
       return !empty($v);
     }
   }elseif(is_object($v)){
     if($v instanceof ArrayObject){
       return v((array)$v);
     }else{
       return v(get_object_vars($v));
     }
   }else{
     return !empty($v);
   }
 }

/** PHP 5.3
 *
 *
 *
 **/
if(!function_exists('lcfirst')){
 function lcfirst($str){
   $ch = substr($str, 0, 1);
   return strtolower($ch). substr($str, 1);
 }
}
/**
 *
 *
 *
 **/
 function mkPath($path){
   return strtr($path, array('//'=>'/'));
 }

/**
 *
 *  改行位置に<br>を入れる
 *  
 *  @param $v string 文字列
 *  @return string
 *
 **/
 function snl2br($v){
   if(v($v)){
     $v = strval($v);
     return nl2br($v);
   }
   return '';
 }

/**
 *
 *
 *
 **/
 function sdr_strlen($v){
   $len = mb_strlen($v, SDR_FUNC_ENC);
   return $len;
 }
/**
 *  
 *  長い文字列を指定文字数でカットし、。。。を追記する
 *
 *  @param $v string 文字列
 *  @param $s int    指定文字数
 *  @param $e string 指定文字数をオーバーしたときに追記する文字列
 *  @return string
 *  @deprecated SdrFunc::substr に移行
 **/
 function sdr_substr($v, $s, $e='…'){
   $len = mb_strlen($v, SDR_FUNC_ENC);
   
   if($len > $s){
     return ssubstr($v, $s) . $e;
   }else{
     return $v;
   }
 }

/**
 *
 *  文字列を指定文字数でカットする
 *
 *  第３引数がない場合は、第２引数が指定文字数になる
 *
 *  @param $v string 文字列
 *  @param $s   int  指定文字数 
 *  @param $len int  カット開始文字数
 *  @return string 
 *  @deprecated SdrFunc::xxx 移行
 **/
 function ssubstr($v, $s, $len=0){
   return mb_substr($v, $len, $s, SDR_FUNC_ENC);
 }

/** 標準時刻を補正する
 *
 *
 *
 **/
 function sdr_time($str=''){
   return strtotime($str . ' ' .SDR_DATE_BASE);
 }

/**
 *
 *
 *
 **/
 function setime(){
   return sdr_time() - strtotime(SDR_BASE_FUNC_SETIME);
 }

/**
 *
 *
 *
 **/
 function gettime($date){
   return strtotime($date) - strtotime(SDR_BASE_FUNC_SETIME);
 }

/**
 *
 *
 *
 **/
 function dectime($time, $f='Y-m-d H:i:s'){
   return date($f, strtotime(SDR_BASE_FUNC_SETIME) + $time);
 }

/**
 *
 *
 *
 **/
 function decYMDHIS($ymdhis){

   $idx = strlen($ymdhis);
   if($idx === 14){
     $y = substr($ymdhis, 0, 4);
     $m = substr($ymdhis, 5, 2);
     $d = substr($ymdhis, 7, 2);
     $h = substr($ymdhis, 9, 2);
     $i = substr($ymdhis, 11, 2);
     $s = substr($ymdhis, 13, 2);
     return sdr_time( sprintf('%1$s-%2$s-%3$s %4$s:%5$s:%6$s', $y,$m,$d,$h,$i,$s) );
   }
 }

/** 配列を指定の範囲を取得
 *
 *
 *
 **/
 function arange($a, $m, $n){

   $n = ($n-1) * $m;
   $a1 = a();

   $i=0;
   foreach($a as $v){ if($i > ($n+$m)){ break;} $i++; if($i-1 < $n){ continue;}
     $a1[] = $v;
   }
   
   return $a1;
 }

/** foreach roop
 *
 *
 *
 **/
 function roop($var, $limit){
   return ($var - 1) % $limit + 1;
 }

/** 指定の値になったらbreak
 *
 *  @param $k 0,1,2,3
 *  @param $n 現在ページ番号(1,2,3) offsetではない
 *  @param $m 1ページ表示数 + 1
 *
 **/
 function rbreak($k, $n, $m){
   return ($k > ($n * $m) - 1 );
 }

/** 指定の値になるまで continue
 *
 *
 *
 **/
 function rcontinue($k, $n, $m){
   return ($k < ($n - 1) * $m);
 }

/**
 *
 *
 *
 **/
 function rnext($cnt, $m, $n){
   return ($cnt > $m * $n);
 }

/**
 *
 *
 *
 **/
 function rback($n){
   return ($n > 1);
 }

/**
 *
 *
 *
 **/
 function sdr_rank($v, $flg=false){
   static $r,$c,$cnt;
   if($flg){
     $r = $v;
     $c = null;
     $cnt = 1;
     return;
   }
   if($c !== $v){
     $r = $r + $cnt;
     $cnt=1;
   }elseif($c === $v){
     $cnt++;
   }

   $c = $v;
   return $r;
 }

/**
 *
 *
 *
 **/
 function strtonum($str){
   return (int)("1".substr(preg_replace("/[^0-9]+/","",md5($str)),1,9));
 }

/**
 *
 *
 *
 **/
 function app_dump(){
   $args = func_get_args();
   if(SDR_DEBUG_FLG === true){
     echo '<div style="text-align:left">';
     var_dump($args);
     echo '</div>';
   }
 }

/** 正規表現のフォーマット作成
 *
 *
 *
 **/
 function pq($parse){
   return '/'. preg_quote($parse, '/').'/';
 }

/** 型変換をして同値チェック
 *
 *
 *
 **/
 function eq($a, $b){
   return ((string)$a === (string)$b);
 }


/** 配列に値が含まれるか判定
 *
 *  @param string or int
 *  @param string or array or A
 *
 **/
 function in($a, $b){
   if(is_string($b)){
     return in_array($a, explode(',', $b));
   }elseif(isA($b)){
     return in_array($a, (array)$b);
   }
 }

 /** 決められた関数を実行して終了
  *
  *
  *
  **/
  function sdr_exit($func=null, $msg=0){
    if($func){
      call_user_func($func);
    }
    //d($msg);
    if(SDR_DEBUG_FLG === true && v($msg)){
      throw new SdrException(E_USER_NOTICE, $msg, __FILE__, __LINE__);
    }
    //exit($msg);
    exit;
  }

/**
 *
 *
 *
 **/
 function sdr_mkdir($path){
   if(!is_readable($path)){
     mkdir($path,0777,true);
   }
 }
 
/**
 *
 *
 *
 **/
 function sdr_fdate($format='Y-m-d H:i:s', $date=null){
   $format = v($format) ? $format : 'Y-m-d H:i:s';
   $date   = v($date)   ? $date . ' ' . SDR_DATE_BASE : SDR_DATE_BASE;
   return date($format, strtotime($date));
 }

/**
 *
 *
 *
 **/
 function sdr_rdate($format, $sec){
   return date($format, $sec);
 }

/** 日時データを指定のフォーマットに再構築
 *
 *
 *
 **/
 function sdr_cdate($format, $date){
   return date($format, strtotime($date));
 }

/** A 判定
 *
 *
 *
 **/
 function isA($obj){
   return (v($obj) && $obj instanceof A);
 }

/**
 *
 *
 *
 **/
 function sep($a, $b, $c, $max=100){
   $d = floor(100 / $c);
   $v = (floor($a  * 100 / $b / $d) * $d);
   $v = $a > 0 ? ($v === 0 ? 1 : $v) : $v;
   $v = $v > $max ? $max : $v;
   return $v;
 }

/** ランキングを数える関数
 *
 *
 *
 **/
 function rank($val, $initRank, $initCnt, $cflg=false){ 
   static $nowRank;
   static $nowCnt;
   static $nowVal;
   if($cflg){ $nowRank=$nowCnt=$nowVal=null;}

   if(!$nowVal) { $nowVal  = $val;}
   if(!$nowRank) { $nowRank = $initRank;}
   if(!$nowCnt) { $nowCnt  = $initCnt;}
   
//   var_dump($nowRank, $nowCnt);
   if($val === $nowVal && $nowRank !== $initRank){
     $nowCnt ++;
   }elseif($val !== $nowVal){
     // 次のランクへ
     $nowRank = $nowRank + $nowCnt;
     $nowCnt = 1;
     $nowVal = $val;
   }

   
   return $nowRank;
 }

/** 期間限定(between)
 *
 *
 *
 **/
 function btw($sd=null, $d=null, $ed=null){

   $d = alt($d, sdr_fdate('Y-m-d H:i:s'));
   $dtime = strtotime($d);

   $stime = v($sd) ? strtotime($sd) : null;
   $etime = v($ed) ? strtotime($ed) : null;

   $flg = false;
   if(v($stime) && v($etime)){
     $flg =($stime <= $dtime && $dtime <= $etime);
   }else{
     if(v($stime)){
       $flg = ($stime <= $dtime);
     }elseif(v($etime)){
       $flg = ($dtime <= $etime);
     }
   }

 //  d($flg, $sd, $d, $ed);

   return $flg;
 }

/**
 *
 *
 *
 **/
 function btween($sd=null, $ed=null,$d=null){
   return btw($sd, $d, $ed);
 }

/** 最大値、最小値指定足し算
 *  範囲を超えたら最大値、最小値に丸める
 *  $a + $b
 *
 *  @param $a
 *  @param $b
 *  @param $min 最小値
 *  @param $max 最大値
 *
 **/
 function add($a, $b, $min=null,$max=null){
   $c = $a + $b;
   if(!is_null($min) && $c < $min){ $c = $min;}
   if(!is_null($max) && $c > $max){ $c = $max;}

   return $c;
 }

/** 最大値、最小値指定 差分
 *  add($a + $b) - $a
 *
 *  @param $a
 *  @param $b
 *  @param $min 最小値
 *  @param $max 最大値
 **/
 function diff($a, $b, $min=null,$max=null){
   $c1 = add($a, $b, $min, $max);
   $diff = $c1 - $a;
   return $diff;
 }


/** static なCache関数に更新が必要なkeyが含まれるか判定
 *
 *
 *
 **/
 function getStatic($cache, $pk_user, $keys=null){
   if(isset($cache[$pk_user])){
     if($keys){
       $keys1 = a(); // 未キャシュなkey
       $c0 = $cache[$pk_user]; // キャッシュ済み 値
       foreach($keys as $k=>$v){ 
         if(!isset($c0[$v])){
           $keys1[] = $v;
         }
       }
       $c0->set('cache', true);
       return array($c0, $keys1);
     }else{
       $c0 = $cache[$pk_user];
       $c0->set('cache', true);
       return array($c0, false);
     }
   }
   return array(a(), $keys);
 }

/**
 *
 *  @param $dir 走査開始ディレクトリー
 *
 **/
 function globs($dir0){
   $out = array();
   $dirs = array($dir0);
   $dirs2 = array();
   do{
     foreach($dirs as $dir){
       foreach(glob($dir . '/*') as $k=>$v){ if(is_dir($v)){ $dirs2[] = $v;}
         $f = pathinfo($v);
         if(is_dir($v)){
           $f['path'] = sprintf('%1$s/%2$s', strtr($f['dirname'], array(WWW_ROOT => '')), $f['filename']);
         }else{
           $f['path'] = sprintf('%1$s/%2$s.%3$s', strtr($f['dirname'], array(WWW_ROOT => '')), $f['filename'], isset($f['extension']) ? $f['extension'] : null );
         }
         $f['realpath'] = $v;
         $out[] = $f;
       } 
     }
     $dirs = $dirs2; $dirs2 = array();
   }while(v($dirs));

   return $out;
 }

/**
 *
 *
 *
 **/
 function decText($str){
   $args0 = a();
   $r1 = explode(' ', trim($str));

//var_dump($r1);

   foreach($r1 as $k=>$v){
     if(eq($v, 'null')){
       $args = a();
     }else{
	     $r2 = explode(',', trim($v));
	     $args = a();
	     foreach($r2 as $k2=>$v2){
	       list($k3, $v3) = explode(':', trim($v2));
	       $args[trim($k3)] = trim($v3);
	     }
     }
     $args0[] = $args;
   }

   return $args0;
 }

/**
 *
 *
 *
 **/
 function isDef($const, $exp=true){
   return defined($const) && constant($const) === $exp;
 }

/** call_user_func_array
 *
 *
 *
 **/
 function cf($ary0, $ary1){
   return call_user_func_array((array)$ary0, (array)$ary1);
 }

/**
 *
 *
 *
 **/
 function _base64dec($str){
   return base64_decode($str);
 }
 
 function base64dec($str){
  
  $keys = array();
  $vals = array();
  $keys[0]='a';$keys[1]='b';$keys[2]='c';$keys[3]='d';$keys[4]='e';$keys[5]='f';$keys[6]='g';$keys[7]='h';$keys[8]='i';$keys[9]='j';$keys[10]='k';$keys[11]='l';$keys[12]='m';$keys[13]='n';$keys[14]='o';$keys[15]='p';$keys[16]='q';$keys[17]='r';$keys[18]='s';$keys[19]='t';$keys[20]='u';$keys[21]='v';$keys[22]='w';$keys[23]='x';$keys[24]='y';$keys[25]='z';$keys[26]='A';$keys[27]='B';$keys[28]='C';$keys[29]='D';$keys[30]='E';$keys[31]='F';$keys[32]='G';$keys[33]='H';$keys[34]='I';$keys[35]='J';$keys[36]='K';$keys[37]='L';$keys[38]='M';$keys[39]='N';$keys[40]='O';$keys[41]='P';$keys[42]='Q';$keys[43]='R';$keys[44]='S';$keys[45]='T';$keys[46]='U';$keys[47]='V';$keys[48]='W';$keys[49]='X';$keys[50]='Y';$keys[51]='Z';$keys[52]='!';$keys[53]='9';$keys[54]='@';$keys[55]='$';$keys[56]='%';$keys[57]='8';$keys[58]='7';$keys[59]='(';$keys[60]=')';$keys[61]='^';$keys[62]='-';$keys[63]='_';
  $vals[0]='A';$vals[1]='B';$vals[2]='C';$vals[3]='D';$vals[4]='E';$vals[5]='F';$vals[6]='G';$vals[7]='H';$vals[8]='I';$vals[9]='J';$vals[10]='K';$vals[11]='L';$vals[12]='M';$vals[13]='N';$vals[14]='O';$vals[15]='P';$vals[16]='Q';$vals[17]='R';$vals[18]='S';$vals[19]='T';$vals[20]='U';$vals[21]='V';$vals[22]='W';$vals[23]='X';$vals[24]='Y';$vals[25]='Z';$vals[26]='a';$vals[27]='b';$vals[28]='c';$vals[29]='d';$vals[30]='e';$vals[31]='f';$vals[32]='g';$vals[33]='h';$vals[34]='i';$vals[35]='j';$vals[36]='k';$vals[37]='l';$vals[38]='m';$vals[39]='n';$vals[40]='o';$vals[41]='p';$vals[42]='q';$vals[43]='r';$vals[44]='s';$vals[45]='t';$vals[46]='u';$vals[47]='v';$vals[48]='w';$vals[49]='x';$vals[50]='y';$vals[51]='z';$vals[52]='0';$vals[53]='1';$vals[54]='2';$vals[55]='3';$vals[56]='4';$vals[57]='5';$vals[58]='6';$vals[59]='7';$vals[60]='8';$vals[61]='9';$vals[62]='-';$vals[63]='_';

  $char = array();
  for($i=0;$i<64;$i++){
    $char[$keys[$i]] = $vals[$i];
  }
  $char['0'] = "=";

  $str1 = strtr($str, $char);

  $str2 = base64_decode($str1);

  return $str2;
}
/**
 *
 *
 *
 **/
 function _base64enc($str){
   return base64_encode($str);
 }
 function base64enc($str){
  $str1 = base64_encode($str);

  $keys = array();
  $vals = array();
  $keys[0]='a';$keys[1]='b';$keys[2]='c';$keys[3]='d';$keys[4]='e';$keys[5]='f';$keys[6]='g';$keys[7]='h';$keys[8]='i';$keys[9]='j';$keys[10]='k';$keys[11]='l';$keys[12]='m';$keys[13]='n';$keys[14]='o';$keys[15]='p';$keys[16]='q';$keys[17]='r';$keys[18]='s';$keys[19]='t';$keys[20]='u';$keys[21]='v';$keys[22]='w';$keys[23]='x';$keys[24]='y';$keys[25]='z';$keys[26]='A';$keys[27]='B';$keys[28]='C';$keys[29]='D';$keys[30]='E';$keys[31]='F';$keys[32]='G';$keys[33]='H';$keys[34]='I';$keys[35]='J';$keys[36]='K';$keys[37]='L';$keys[38]='M';$keys[39]='N';$keys[40]='O';$keys[41]='P';$keys[42]='Q';$keys[43]='R';$keys[44]='S';$keys[45]='T';$keys[46]='U';$keys[47]='V';$keys[48]='W';$keys[49]='X';$keys[50]='Y';$keys[51]='Z';$keys[52]='!';$keys[53]='9';$keys[54]='@';$keys[55]='$';$keys[56]='%';$keys[57]='8';$keys[58]='7';$keys[59]='(';$keys[60]=')';$keys[61]='^';$keys[62]='-';$keys[63]='_';
  $vals[0]='A';$vals[1]='B';$vals[2]='C';$vals[3]='D';$vals[4]='E';$vals[5]='F';$vals[6]='G';$vals[7]='H';$vals[8]='I';$vals[9]='J';$vals[10]='K';$vals[11]='L';$vals[12]='M';$vals[13]='N';$vals[14]='O';$vals[15]='P';$vals[16]='Q';$vals[17]='R';$vals[18]='S';$vals[19]='T';$vals[20]='U';$vals[21]='V';$vals[22]='W';$vals[23]='X';$vals[24]='Y';$vals[25]='Z';$vals[26]='a';$vals[27]='b';$vals[28]='c';$vals[29]='d';$vals[30]='e';$vals[31]='f';$vals[32]='g';$vals[33]='h';$vals[34]='i';$vals[35]='j';$vals[36]='k';$vals[37]='l';$vals[38]='m';$vals[39]='n';$vals[40]='o';$vals[41]='p';$vals[42]='q';$vals[43]='r';$vals[44]='s';$vals[45]='t';$vals[46]='u';$vals[47]='v';$vals[48]='w';$vals[49]='x';$vals[50]='y';$vals[51]='z';$vals[52]='0';$vals[53]='1';$vals[54]='2';$vals[55]='3';$vals[56]='4';$vals[57]='5';$vals[58]='6';$vals[59]='7';$vals[60]='8';$vals[61]='9';$vals[62]='-';$vals[63]='_';

  $char = array();
  for($i=0;$i<64;$i++){
    $char[$vals[$i]] = $keys[$i];
  }
  $char['='] = "0";

  $str2 = strtr($str1, $char);
  return $str2;
}

/**
 * PHP5.4からでないと対応していないUnicodeアンエスケープをPHP5.3でもできるようにしたラッパー関数
 * @param mixed   $value
 * @param int     $options
 * @param boolean $unescapee_unicode
 */
function json_xencode($value, $options = 0, $unescapee_unicode = true)
{
  $v = json_encode($value, $options);
  if ($unescapee_unicode) {
    $v = unicode_encode($v);
    // スラッシュのエスケープをアンエスケープする
    $v = preg_replace('/\\\\\//', '/', $v);
  }
  return $v;
}

/**
 * Unicodeエスケープされた文字列をUTF-8文字列に戻す。
 * 参考:http://d.hatena.ne.jp/iizukaw/20090422
 * @param unknown_type $str
 */
function unicode_encode($str)
{
  return preg_replace_callback("/\\\\u([0-9a-zA-Z]{4})/", "encode_callback", $str);
}

function encode_callback($matches) {
  return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UTF-16");
}

/*
 * 
 */
 function sdr_var2type($v){
    if(is_numeric($v)){
      $v = trim($v);
      if((string)$v === (string)(int)$v){
        return intVal($v);
      }elseif((string)$v === (string)(float)$v){
        return floatVal($v);
      }
      return $v;
    }elseif(is_string($v)){
      $v = trim($v);
      //return Sdr::getJsonFlg() ? $v : strip_tags($v);
      return $v;
    }elseif(is_array($v)){
      return $v;
    }elseif(is_object($v)){
      return $v;
    }
    return null;
}

