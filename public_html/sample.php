<?php
/*
 * 
 */
# Includes the autoloader for libraries installed with composer
require __DIR__ . '/../vendor/autoload.php';

# Imports the Google Cloud client library
use Google\Cloud\Datastore\DatastoreClient;

# Your Google Cloud Platform project ID
$projectId = 'test-tk-150723';
$namespaceId = 'bushimichi';

# Instantiates a client
$datastore = new DatastoreClient([
    'projectId' => $projectId,
    'namespaceId'=>$namespaceId,
]);

# The kind for the new entity
$kind = 'Task';

# The name/ID for the new entity
$name2 = 'sampletask1';
$name = 'aaa-b=444,pk_user=1';

# The Cloud Datastore key for the new entity
$taskKey = $datastore->key($kind, $name);

# Prepares the new entity
$task = $datastore->entity($taskKey, ['description' => 'Buy milk2']);

# Saves the entity
$datastore->upsert($task);

echo 'Saved ' . $task->key() . ': ' . $task['description'] . PHP_EOL;

$taskKey2 = $datastore->key($kind, $name2);
$datastore->delete($taskKey2);

