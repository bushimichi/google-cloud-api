<?php
/*
 * 
 */
require_once(__DIR__ . "/lib/sdr_action.php");
/*
 * 
 */
class UserAction extends SdrAction{
	
	/*
	 * 
	 */
	protected $res = [];
	
	/*
	 * 
	 */
	private function entryAction(Req $Req){
		
		
		
		$this->res = [];
	}
	
	/*
	 * 
	 */
	public static function main(){
		
        $Req = Req::getNew();
		$Req = Req::decParam($Req);

		//----------------------------------------------------------------------------
		$method = $Req->method ?: strtoupper($_SERVER['REQUEST_METHOD']);
		if(eq($_SERVER['HTTP_DEBUG_DHC'],1)){
		  $method = $_POST["method"] ?: $method;
		}

		$act  = sprintf('%1$sAction', $Req->act);
		if(!method_exists(self,$act)){ exit; }

        $inst = new self();
        $inst->$act($Req);
        $inst->printJson();
	}
}