<?php
require_once("./lib/Sdr.php");
/*
 * 
 */
if(!defined('APP_ACT')){ exit; }
/*
 * 
 */
$action_php = sprintf(__DIR__.'/%1$s_action.php', APP_ACT);
if(!file_exists($action_php)){ exit; }
/*
 * 
 */
require_once($action_php);
/*
 * 
 */
use com\itigm\gae;
$action_class = sprintf('%1$sAction', ucfirst(APP_ACT));
$action_class::main();
// EOP