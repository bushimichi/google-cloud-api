<?php
/**
 * 
 */
class QueryAction{
	
	public static function main(){
		
		$Req = Req::getNew();
		$Req = Req::decParam($Req);

		//----------------------------------------------------------------------------
		$method = $Req->method ?: strtoupper($_SERVER['REQUEST_METHOD']);
		if(eq($_SERVER['HTTP_DEBUG_DHC'],1)){
		  $method = $_POST["method"] ?: $method;
		}

		$tbl  = $Req->tbl;
		$pkey = $Req->pkey;

		/*
		 * 
		 */
		$param = [];
		foreach($Req->getRequestPostKeys() as $k=>$v){
			if(!eq($v,'pkey')){
			  $param[$v] = $Req->$v;
		    }
		}
		//----------------------------------------------------------------------------
		//d(APP_NAME);
		//d($method);
		//d($_SERVER);
		//$msg = $Req->getParam();
		$res = [];
		$res['APP_ID'] = APP_ID;
		$res['APP_NS'] = APP_NS;
		$res['method'] = $method;
		$res['param'] = $Req->getParam();
		$res['tbl'] = $tbl;
		$res['pkey'] = $pkey;
		//
		if(eq($method,"QUERY") && v($tbl)){
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  $res['rows'] = [$ds->lookup($pkey)];
		  
		}elseif(eq($method,"POST") && v($pkey) && v($tbl)){
			
		  $param["indate"]=date('Y-m-d H:i:s');
		  $param["chdate"]=date('Y-m-d H:i:s');
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  $res['status'] = $ds->insert($pkey, $param);
		  
		}elseif(eq($method,"PUT") && v($pkey) && v($tbl)){
		  $add = ["indate"=>date('Y-m-d H:i:s')];
		  $param["chdate"]=date('Y-m-d H:i:s');
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  $res['status'] = $ds->update($pkey, $param, $add);
			
		}elseif(eq($method,"DELETE") && v($pkey) && v($tbl)){
		  $ds = new DataStore(APP_ID, APP_NS, $tbl);
		  $res['status'] = $ds->delete($pkey);
		}
		
		/*
		 * 
		 */
		header('Api-Version: ' . API_VER);
		
		/*
		 * 
		 */
		if(eq($_SERVER['HTTP_DEBUG_DHC'],1)){
		   	 header("Content-Type: application/json; charset=utf-8");
		     echo json_encode($res,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);	
		}else{
		
		   if(eq(API_VER,2)){
		     $json = json_encode($res,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
		     $json = urlencode($json);
		     $json2 = base64enc($json);
		     header("Content-Type: text/plain; charset=utf-8");
		     echo $json2;
		   }else{
		   	 header("Content-Type: application/json; charset=utf-8");
		     echo json_encode($res,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		   }
		}
	}

} // EOC
// EOP